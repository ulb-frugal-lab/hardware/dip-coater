## Links :

* [Wiki](https://gitlab.com/ulb-frugal-lab/hardware/dip-coater/wikis/home)

## To do :

* [x] Upload code and CAD files to repo
* [ ] Add assembly pages to the wiki
* [ ] Reference Arduino code libraries and credit the authors