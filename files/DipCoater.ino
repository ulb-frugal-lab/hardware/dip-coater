#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <ClickEncoder.h>
#include <TimerOne.h>

#define stp 11	// EasyDriver STEP input
#define dir 12	// EasyDriver DIR. input

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

ClickEncoder *encoder;
void timerIsr() {
  encoder->service();
}

const int refDelay = 2500;
int dipSpeed = 5, last = 0;
unsigned int pulse = refDelay / dipSpeed;

void setup() {

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  display.display();
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(2);
  display.setCursor(0, 0);
  display.print("Speed");
  display.setTextSize(3);
  display.setCursor(0, 30);
  display.print(dipSpeed);
  display.setCursor(50, 30);
  display.print("mm/s");

  display.display();

  encoder = new ClickEncoder(2, 3, 4, 4);
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);

  pinMode(5, INPUT_PULLUP);	// Dip/Rise button
  pinMode(6, INPUT_PULLUP);	// Lower switch
  pinMode(7, INPUT_PULLUP);	// Upper switch
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  digitalWrite(11, LOW);
  digitalWrite(12, LOW);
}

void loop() {
  dipSpeed += encoder->getValue();
  if (dipSpeed != last) {
    if (dipSpeed < 1)
    {
      dipSpeed = 1;
    }
    else if (dipSpeed > 60)
    {
      dipSpeed = 60;
    }

    display.clearDisplay();
    display.setTextColor(WHITE);
    display.setTextSize(2);
    display.setCursor(0, 0);
    display.print("Speed");
    display.setTextSize(3);
    display.setCursor(0, 30);
    display.print(dipSpeed);
    display.setCursor(50, 30);
    display.print("mm/s");

    display.display();

    pulse = refDelay / dipSpeed;
    last = dipSpeed;
  }

  if (!digitalRead(5)) {		// Dip/Rise push-button	
    if (digitalRead(6)) {		// Lower switch
      digitalWrite(12, HIGH);	// Go up
      while (!digitalRead(7)) {
        fullStep();
      }
    }
    else if (digitalRead(7)) {	// Upper switch
      digitalWrite(12, LOW);	// Go down
      while (!digitalRead(6)) {
        fullStep();
      }
    }
  }
  
  delay(1);
}

void fullStep() {
  for (int i = 0; i < 8; i++) {
    digitalWrite(11, HIGH);
    delayMicroseconds(pulse);
    digitalWrite(11, LOW);
    delayMicroseconds(pulse);
  }
}